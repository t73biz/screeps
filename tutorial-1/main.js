var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');

module.exports.loop = function () {

    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
        }
    }
    var creepBody = [WORK,CARRY,CARRY,MOVE,MOVE];
    var builders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder');
    var harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
    var upgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader');

    if(builders.length < 2 && Game.spawns.Capital.energy > 299) {
        Game.spawns.Capital.createCreep(creepBody, undefined, {role: 'builder'});
    }

    if(harvesters.length < 2 && Game.spawns.Capital.energy > 299) {
        Game.spawns.Capital.createCreep(creepBody, undefined, {role: 'harvester'});
    }

    if(upgraders.length < 2 && Game.spawns.Capital.energy > 299) {
        Game.spawns.Capital.createCreep(creepBody, undefined, {role: 'upgrader'});
    }

    for(var creepName in Game.creeps) {
        var creep = Game.creeps[creepName];
        if(creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        if(creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
    }
}