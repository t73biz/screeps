import { Creep, Flag, Energy } from 'screeps-globals';

export default class Foreman {
    var game;
    var minPoolSize = 10;
    var roleBuilder   = require('Workers/Builder');
    var roleHarvester = require('Workers/Harvester');
    var rolePaver     = require('Workers/Paver');
    var roleUpgrader  = require('Workers/Upgrader');
    var roleWaller    = require('Workers/Waller');

    constructor(game) {
        this.game = game;
    }

    supervise() {

    }

    getPool() {
        return _.filter(this.game.creeps, (creep) => creep.memory.type == 'Worker');
    }
}