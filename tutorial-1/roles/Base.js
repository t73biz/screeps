import { Creep, Flag, Energy } from 'screeps-globals';

export default class Base extends Creep {
    constructor(creep) {
        super(creep.id);
        this.setRole();
    }

    getRole() {
        return this.memory.role;
    }

    setRole() {
        this.memory.role = role;

        return this;
    }

    getType() {
        return this.memory.type;
    }

    setType() {
        this.memory.type = type;

        return this;
    }

    preformRole() {
        console.log(`WHOA! ${this.getRole()} does not have a performRole implementation!!!`);
    }
}