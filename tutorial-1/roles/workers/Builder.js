import Worker from 'Worker';

export default class Builder extends Worker {
    var role = 'Builder';

    preformRole() {
        console.log(`WHOA! ${this.getRole()} does not have a performRole implementation!!!`);
    }
}