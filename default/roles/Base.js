import { Creep, Flag, Energy } from 'screeps-globals';

export default class Base extends Creep {
    constructor(creep) {
        super(creep.id);
        this.setRole();
    }

    getRole() {
        return this.memory.role;
    }

    setRole() {
        this.memory.role = '';

        return this;
    }

    preformRole() {
        console.log(`WHOA! ${this.getRole()} does not have a performRole implementation!!!`);
    }
}