import Worker from '../Worker';

export default class Harvester extends Worker {
    getRole() {
        return 'harvester';
    }
}