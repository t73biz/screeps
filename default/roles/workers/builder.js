import Worker from '../Worker';

export default class Builder extends Worker {
    var roleType = 'Builder';

    preformRole() {
        console.log(`WHOA! ${this.getRole()} does not have a performRole implementation!!!`);
    }
}