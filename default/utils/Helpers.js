import { RoomPosition } from 'screeps-globals';

Object.assign(RoomPosition.prototype, {
    identifier() {
        return '${this.roomName}x${this.x}y${this.y}';
    },

    actualDistanceTo(pos) {
        return Math.sqrt(Math.pow(this.x - pos.x, 2) + Math.pow(this.y - pos.y, 2));
    }
});